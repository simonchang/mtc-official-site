<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <title></title>
    <link type="text/css" href="menu.css" rel="stylesheet" />
    <script type="text/javascript" src="jquery.js"></script>
    <script type="text/javascript" src="menu.js"></script>
</head>
<body>

<style type="text/css">
* { margin:0;
    padding:0;
}
html { background:#b6b7bc; }
body {
    margin:0px auto;
    width:805px;
    height:1500px;
    overflow:hidden;
    background:#fff url(images/back.jpg) no-repeat;
}
div#menu {
    margin:64px 0 0 0px;
    position:absolute;
}
div#jquery {
    font:11px 'Trebuchet MS';
    color:#fff;
    text-align:center;
    clear:left;
    position:absolute;
    top:546px;
    width:560px;
}
div#jquery a { color:#ffffff; }
div#jquery a:hover { color:#fff; }

ul, ol, dl { 
	padding: 0;
	margin: 0;
}
h1, h2, h3, h4, h5, h6, p {
	margin-top: 0;	 
	padding-right: 15px;
	padding-left: 15px; 
}
a img { 
	border: none;
}

a:link {
	color: #42413C;
	text-decoration: underline; 
}
a:visited {
	color: #6E6C64;
	text-decoration: underline;
}
a:hover, a:active, a:focus { 
	text-decoration: none;
}

.container {
	width: 800px;
	background-color: #000000;
	margin: 0 auto; 
}

header {
	background-color: #363636;
}

.sidebar1 {
	float: left;
	width: 180px;
	background-color: #EADCAE;
	padding-bottom: 10px;
}
.content {
	padding: 10px 0;
	width: 600px;
	float: left;
}
aside {
	float: left;
	width: 180px;
	background-color: #EADCAE;
	padding: 10px 0;
}


.content ul, .content ol {
	padding: 0 15px 15px 40px; 
}


ul.nav {
	list-style: none; 
	border-top: 1px solid #666; 
	margin-bottom: 15px; 
}
ul.nav li {
	border-bottom: 1px solid #666; 
}
ul.nav a, ul.nav a:visited { 
	padding: 5px 5px 5px 15px;
	display: block; 
	width: 160px;  
	text-decoration: none;
	background-color: #C6D580;
}
ul.nav a:hover, ul.nav a:active, ul.nav a:focus { 
	background-color: #ADB96E;
	color: #FFF;
}

/* ~~ 頁尾 ~~ */
footer {
	padding: 10px 0;
	background-color: #CCC49F;
	position: relative;
	clear: both;
}
/* ~~ 其他 float/clear 類別 ~~ */
.fltrt {  
	float: right;
	margin-left: 8px;
}
.fltlft { 
	float: left;
	margin-right: 8px;
}
.clearfloat { 
	clear:both;
	height:0;
	font-size: 1px;
	line-height: 0px;
}

/*HTML 5 支援 – 設定新的 HTML 5 標籤以顯示區塊並使瀏覽器能夠正確顯示標籤。 */
header, section, footer, aside, article, figure {
	display: block;
}
-->
</style>
<div id="header">    
<div id="menu">   
	<ul class="menu">        
        <li><a href="http://macrotech.herokuapp.com/"><span>首頁</span></a></li>
        <li><a href="content.php" target="_self"><span>重大歷程</span></a></li>
		<li><a href="content-is.php" target="_self"><span>資訊工程</span></a></li>
		<li><a href="content-green.php" target="_self"><span>綠能機電</span></a></li>
		<li><a href="content-eng.php" target="_self"><span>機電工程</span></a></li>
		<li><a href="content-sales.php" target="_self"><span>業務</span></a></li>
		<li><a href="http://www.104.com.tw/jobbank/custjob/index.php?r=cust&j=4070422648363f683c583a1d1d1d1d5f2443a363189j48&jobsource=checkc" target="_new"><span>人力資源</span></a></li>
		<li><a href="contact-us.php" target="_self"><span>連絡我們</span></a></li>
        <li><a href="#"><span>　　　　　</span></a></li>
        <li><a href="#"><span>　　　　　</span></a></li>    	
    </ul> 	
    </ul>
</div><br><br><br><br><br><br><br>
<div class="main" style="overflow: auto; border: solid 0px red; scrollbar-face-color: white; font-size: 12px; text-align: left; font-family: Arial, Helvetica, sans-serif;">
            
    <div id="MainContent_UpdatePanel1">
	
            <div id="MainContent_div_List">
                <fieldset>
                    <legend>                    <strong><img src="http://www.so-power.com/Img/point.gif" alt="" style="width: 17px; height: 15px" />綠能機電部</strong>:
                    <br>
                    <br>
                  </legend>
                    <table width="98%" border="1" align="center" cellpadding="1" cellspacing="1">
                      <tbody>
                        <tr>
                          <td valign="top" width="172"><p><span lang="EN-US"><u></u></span><strong><img src="http://www.so-power.com/Img/point.gif" alt="" style="width: 17px; height: 15px" /></strong>綠能機電部</p></td>
                          <td valign="top" width="297"><p>營業能力服務項目 <span lang="EN-US"><u></u><u></u></span></p></td>
                          <td valign="top" width="142"><p><span lang="EN-US"><span style="text-align: center"> </span></span><span style="text-align: center">English Reference</span></p></td>
                        </tr>
                        <tr>
                          <td height="160" valign="top"><img src="http://macrotech.herokuapp.com/00017.jpg" width="172" height="156" /></td>
                          <td valign="top"><p><img src="http://www.so-power.com/Img/point.gif" alt="" style="width: 17px; height: 15px" />綠建築<span lang="EN-US"> - </span>智慧節能光源 、太陽光電系統工程規劃設計施工監造<br>
                            <br>
                            <img src="http://www.so-power.com/Img/point.gif" alt="" style="width: 17px; height: 15px" />無塵室廠房機電設計規劃與施工承造<br>
                            <br>
                            <a href="http://macrotech.herokuapp.com/00020.jpg" target="_new">實績照片1</a> <br>
                            <a href="http://macrotech.herokuapp.com/00021.jpg" target="_new">實績照片2</a><br>
                            <a href="http://macrotech.herokuapp.com/00022.jpg" target="_new">實績照片3</a><br>
                          </p></td>
                          <td valign="top"><p><span lang="EN-US">Green construction – energy saving </span></p>
                            <p><span lang="EN-US">Solar system –design and engineering .</span></p>
                          <p><span lang="EN-US">Cleaning room electric engineering and construction.</span><span lang="EN-US"> </span></p></td>
                        </tr>
                        <tr>
                          <td width="172" height="160" valign="top"><p><img src="http://www.so-power.com/Img/point.gif" alt="" style="width: 17px; height: 15px" />綠能工程課介紹:</p>
                            <p>綠能產業定義:</p>
                            <p>綠色能源主要指潔淨能源，舉凡對環境友善的能源使用種類及方式都含在綠能的定義中。</p>
                            <p>狹義的綠色能源指對環境友善的再生能源，如太陽能、風能、地熱能、水資源、生質能及海洋能等<br>
                              ；以廣義來看，綠色能源則包括在能源的生產，以及消費的過程中 選用對生態環境低汙染的能源，如:
                          水、天然氣、淨煤及核能等。</p></td>
                          <td valign="top" width="297"><p><img src="http://www.so-power.com/Img/point.gif" alt="" style="width: 17px; height: 15px" />業務範圍:</p>
                            <p>綠建築 - 智慧節能光源 、太陽光電系統工程規劃設計施工監造</p>
                            <p>1.建築物整合太陽光電系統設計諮詢</p>
                            <p>2.技術諮詢、系統線路設計，以及機電整合規劃</p>
                            <p>3.建築物週邊環境分析，以及建築物陰影模擬</p>
                            <p>4.陰影模擬、分析，以及產電效益預估</p>
                            <p>5.客製化服務，依據個別需求規劃全面性的安裝建議</p>
                            <p>6.投資概算，預期產電效益分析</p>
                            <p>7.市電併聯、相關補助，以及躉售電價申等行政申請文件撰寫</p>
                            <p>8.最佳化零組件選用建議及供貨</p>
                            <p>9.專業系統安裝、施工</p>
                            <p>10.系統定期保養、調整，以及後續之保固、維修</p>
                            <p>11.獨家研發之遠端線上監控工具PV-Control</p>
                            <p>以科技實現傳統「開源節流」的觀念，以LED照明及太陽能來達到「節省能源」，同時「創造能
                          源」的成效。</p></td>
                          <td valign="top" width="142"><p>&nbsp;</p></td>
                        </tr>
                      </tbody>
                  </table>
                  <legend><br>
                  </legend>
                </fieldset><br>
<div class="footer">Copyright &copy; 1986 - 2012 旭宣系統科技(股)公司</div>
            </div>
        
</div>

        </div>
</div>
<div id="jquery"><a href="http://apycom.com/">.</a></div>
</body>
</html>