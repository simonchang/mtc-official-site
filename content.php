<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <title></title>
    <link type="text/css" href="menu.css" rel="stylesheet" />
    <script type="text/javascript" src="jquery.js"></script>
    <script type="text/javascript" src="menu.js"></script>
</head>
<body>

<style type="text/css">
* { margin:0;
    padding:0;
}
html { background:#b6b7bc; }
body {
    margin:0px auto;
    width:805px;
    height:1500px;
    overflow:hidden;
    background:#fff url(images/back.jpg) no-repeat;
}
div#menu {
    margin:64px 0 0 0px;
    position:absolute;
}
div#jquery {
    font:11px 'Trebuchet MS';
    color:#fff;
    text-align:center;
    clear:left;
    position:absolute;
    top:546px;
    width:560px;
}
div#jquery a { color:#ffffff; }
div#jquery a:hover { color:#fff; }

ul, ol, dl { 
	padding: 0;
	margin: 0;
}
h1, h2, h3, h4, h5, h6, p {
	margin-top: 0;	 
	padding-right: 15px;
	padding-left: 15px; 
}
a img { 
	border: none;
}

a:link {
	color: #42413C;
	text-decoration: underline; 
}
a:visited {
	color: #6E6C64;
	text-decoration: underline;
}
a:hover, a:active, a:focus { 
	text-decoration: none;
}

.container {
	width: 800px;
	background-color: #000000;
	margin: 0 auto; 
}

header {
	background-color: #363636;
}

.sidebar1 {
	float: left;
	width: 180px;
	background-color: #EADCAE;
	padding-bottom: 10px;
}
.content {
	padding: 10px 0;
	width: 600px;
	float: left;
}
aside {
	float: left;
	width: 180px;
	background-color: #EADCAE;
	padding: 10px 0;
}


.content ul, .content ol {
	padding: 0 15px 15px 40px; 
}


ul.nav {
	list-style: none; 
	border-top: 1px solid #666; 
	margin-bottom: 15px; 
}
ul.nav li {
	border-bottom: 1px solid #666; 
}
ul.nav a, ul.nav a:visited { 
	padding: 5px 5px 5px 15px;
	display: block; 
	width: 160px;  
	text-decoration: none;
	background-color: #C6D580;
}
ul.nav a:hover, ul.nav a:active, ul.nav a:focus { 
	background-color: #ADB96E;
	color: #FFF;
}

/* ~~ 頁尾 ~~ */
footer {
	padding: 10px 0;
	background-color: #CCC49F;
	position: relative;
	clear: both;
	text-align:center;
}
/* ~~ 其他 float/clear 類別 ~~ */
.fltrt {  
	float: right;
	margin-left: 8px;
}
.fltlft { 
	float: left;
	margin-right: 8px;
}
.clearfloat { 
	clear:both;
	height:0;
	font-size: 1px;
	line-height: 0px;
}

/*HTML 5 支援 – 設定新的 HTML 5 標籤以顯示區塊並使瀏覽器能夠正確顯示標籤。 */
header, section, footer, aside, article, figure {
	display: block;
}
-->
</style>
<div id="header">    
<div id="menu">   
	<ul class="menu">        
        <li><a href="http://macrotech.herokuapp.com/"><span>首頁</span></a></li>
        <li><a href="content.php" target="_self"><span>重大歷程</span></a></li>
		<li><a href="content-is.php" target="_self"><span>資訊工程</span></a></li>
		<li><a href="content-green.php" target="_self"><span>綠能機電</span></a></li>
		<li><a href="content-eng.php" target="_self"><span>機電工程</span></a></li>
		<li><a href="content-sales.php" target="_self"><span>業務</span></a></li>
		<li><a href="http://www.104.com.tw/jobbank/custjob/index.php?r=cust&j=4070422648363f683c583a1d1d1d1d5f2443a363189j48&jobsource=checkc" target="_new"><span>人力資源</span></a></li>
		<li><a href="contact-us.php" target="_self"><span>連絡我們</span></a></li>
        <li><a href="#"><span>　　　　　</span></a></li>
        <li><a href="#"><span>　　　　　</span></a></li>    	
    </ul>
</div><br><br><br><br><br><br><br><div class="main" style="overflow: auto; border: solid 0px red; scrollbar-face-color: white; font-size: 12px; text-align: left; font-family: '新細明體';">
            
    <div id="MainContent_UpdatePanel1">
	
            <div id="MainContent_div_List">
                <fieldset>
                    <legend>                    <strong><img src="http://www.so-power.com/Img/point.gif" alt="" style="width: 17px; height: 15px" />旭宣公司簡介</strong></legend><br>
高新明董事長於1986年9月成立旭宣系統科技股份有限公司。 初期以電腦化Layout設計，
電路服務為主要營業項目。因應台灣在電子業與半導體業的快速發展，便引進國外高科技電子、電腦軟硬體、資訊等設備的代理服務業務。以服務國內科技產業，提昇國內
電子資訊產業之品質與能力。<br>
<br>
                  
                    多年來累積各業種的人脈與電子資訊產業的資源, 旭宣更
因應網際網路與國際化的趨勢，在東南亞地區開始進行多邊商品的國際貿易與增加網路
電子商務營業服務。從2011年開始更積極的進入機電工程與綠能相關產業領域，含跨led
節能燈具的代理銷售，大樓豪宅的整體機電設計規劃監造施工，太陽能發電系統的規劃，
設計工程監造，電信機房的設計施工與系統的整合，與科技廠房機房、無塵室等機電空調等工程 。
                    <br>
                    <br>
                  近幾年來, 旭宣在多元性服務中, 調整組織培育各業種人才精英,期在詭
變與競爭激烈的市場下, 提供給客戶全方位滿意的服務。<br>
<br>
<strong><img src="http://www.so-power.com/Img/point.gif" alt="" style="width: 17px; height: 15px" />公司沿革</strong><br>
                  
                    <table class="TableStyle" width="99%" align="center">
                        <tr>
                            <td valign="top" rowspan="2" width="190px">
                                <table cellpadding="3" cellspacing="0" border="0" style="border-color:Ivory;" width="100%">
                                    <tr>
                                        <td align="left">
                                            <img src="http://www.so-power.com/Img/S1.jpg" style="width: 200px; height: 210px" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" colspan="2">
                                <table width="100%" cellspacing="1">
                                    <tr>
                                        <td valign="top">
                                      <strong>草創設立期:</strong></td>
                                    </tr>
                                    <tr>
                                      <td align="left" class="style2"><span style="color: #336699"><img src="http://www.so-power.com/Img/point.gif" alt="" style="width: 17px; height: 15px" />1986</span> 成立電腦化layout 設計與進出口貿易。</td>
                                    </tr>
                                    <tr>
                                        <td align="left" class="style2"><img src="http://www.so-power.com/Img/point.gif" style="width: 17px; height: 15px" /><span class="style1"><span id="MainContent_Label13" style="color: #336699">1992</span></span> 電腦Layout 軟體PC化 、開始PC 與零件貿易。</td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" valign="top">
                                            <hr width="100%" />
                                        </td>
                                    </tr>
                                    <tr>
                                      <td align="left" class="style2"><strong>區域擴展期:</strong></td>
                                    </tr>
                                    <tr>
                                      <td align="left" class="style2"><span style="color: #336699"><img src="http://www.so-power.com/Img/point.gif" alt="" style="width: 17px; height: 15px" />1994</span> 成立國內PC銷售部門。</td>
                                    </tr>
                                    <tr>
                                      <td align="left" class="style2"><img src="http://www.so-power.com/Img/point.gif" style="width: 17px; height: 15px" /><span class="style1"><span id="MainContent_Label9" style="color: #336699">1997</span></span> 導入國際知名廠商產品銷售，提供軟硬體整合服務。以Microsoft solution
                                      為主。</td>
                                    </tr>
                                    <tr>
                                      <td align="left" class="style2"><img src="http://www.so-power.com/Img/point.gif" alt="" style="width: 17px; height: 15px" /><span class="style1"><span id="MainContent_Label11" style="color: #336699">1999 </span></span>確立以客服為導向 ，銷售知名品牌之伺服器(HP、IBM、Acer )。</td>
                                    </tr>
                                    <tr>
                                      <td align="left" class="style2"><img src="http://www.so-power.com/Img/point.gif" style="width: 17px; height: 15px" /><span class="style1"><span id="MainContent_Label7" style="color: #336699">2002</span></span> 擴充新竹、台南服務點， 提供資訊及網路系統基礎架構建置服務 。</td>
                                    </tr>
                                    <tr>
                                      <td align="left" class="style2"><img src="http://www.so-power.com/Img/point.gif" alt="" style="width: 17px; height: 15px" /><span class="style1"><span id="MainContent_Label12" style="color: #336699">2006 </span></span>提供國內網路銷售行銷服務與引進國際商品行銷服務。</td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" valign="top">
                                            <hr width="100%" />
                                        </td>
                                    </tr>
                                    <tr>
                                      <td valign="top"><strong>多元成長期:</strong></td>
                                    </tr>
                                    <tr>
                                        <td valign="top"><span class="style2"><img src="http://www.so-power.com/Img/point.gif" alt="" style="width: 17px; height: 15px" /><span class="style1"><span id="MainContent_Label8" style="color: #336699">2011</span></span></span> 增加弱電與節能太陽能工程事業 ，提供客戶整合資訊商品的加值服務。</td>
                                    </tr>
                                    <tr>
                                      <td align="left" class="style2">辦理現金增資12,800,000元，增資後資本額為29,800,000元。</td>
                                    </tr>
                                    <tr>
                                      <td align="left" class="style2">積極引進代理加值性商品: 南亞led 燈具; 杜邦專業石材保養防護產 品 。EPSON 自動化手臂(ROBOT)。</td>
                                    </tr>
                                    <tr>
                                        <td align="left" class="style2">開發多媒體電視購物通路擴大網購團購商品。並投資上海吉懋公司作網購銷<br></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" valign="top">
                                            <hr width="100%" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top"><span class="style2"><img src="http://www.so-power.com/Img/point.gif" alt="" style="width: 17px; height: 15px" /><span class="style1"><span id="MainContent_Label10" style="color: #336699">2012</span></span></span> 豪宅機電工程團隊 ，參與基隆與屏潮鐵工局車站改建工程。</td>
                                    </tr>
                                    <tr>
                                        <td align="left" class="style2">整合集團資源 ，承接台北市光纖機房規劃建制工程。</td>
                                    </tr>
                                    
                                    <tr>
                                        <td colspan="2" valign="top">
                                            <hr width="100%" />
                                        </td>
                                    </tr>
                                    
                                </table>
                            </td>
                        </tr>
                    </table>
                </fieldset><br>
<div class="footer">Copyright &copy; 1986 - 2012 旭宣系統科技(股)公司</div>
            </div>
        
</div>

        </div>
</div>
<div id="jquery"><a href="http://apycom.com/">.</a></div>
</body>
</html>