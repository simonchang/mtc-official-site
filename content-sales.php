<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <title></title>
    <link type="text/css" href="menu.css" rel="stylesheet" />
    <script type="text/javascript" src="jquery.js"></script>
    <script type="text/javascript" src="menu.js"></script>
</head>
<body>

<style type="text/css">
* { margin:0;
    padding:0;
}
html { background:#b6b7bc; }
body {
    margin:0px auto;
    width:805px;
    height:1500px;
    overflow:hidden;
    background:#fff url(images/back.jpg) no-repeat;
}
div#menu {
    margin:64px 0 0 0px;
    position:absolute;
}
div#jquery {
    font:11px 'Trebuchet MS';
    color:#fff;
    text-align:center;
    clear:left;
    position:absolute;
    top:546px;
    width:560px;
}
div#jquery a { color:#ffffff; }
div#jquery a:hover { color:#fff; }

ul, ol, dl { 
	padding: 0;
	margin: 0;
}
h1, h2, h3, h4, h5, h6, p {
	margin-top: 0;	 
	padding-right: 15px;
	padding-left: 15px; 
}
a img { 
	border: none;
}

a:link {
	color: #42413C;
	text-decoration: underline; 
}
a:visited {
	color: #6E6C64;
	text-decoration: underline;
}
a:hover, a:active, a:focus { 
	text-decoration: none;
}

.container {
	width: 800px;
	background-color: #000000;
	margin: 0 auto; 
}

header {
	background-color: #363636;
}

.sidebar1 {
	float: left;
	width: 180px;
	background-color: #EADCAE;
	padding-bottom: 10px;
}
.content {
	padding: 10px 0;
	width: 600px;
	float: left;
}
aside {
	float: left;
	width: 180px;
	background-color: #EADCAE;
	padding: 10px 0;
}


.content ul, .content ol {
	padding: 0 15px 15px 40px; 
}


ul.nav {
	list-style: none; 
	border-top: 1px solid #666; 
	margin-bottom: 15px; 
}
ul.nav li {
	border-bottom: 1px solid #666; 
}
ul.nav a, ul.nav a:visited { 
	padding: 5px 5px 5px 15px;
	display: block; 
	width: 160px;  
	text-decoration: none;
	background-color: #C6D580;
}
ul.nav a:hover, ul.nav a:active, ul.nav a:focus { 
	background-color: #ADB96E;
	color: #FFF;
}

/* ~~ 頁尾 ~~ */
footer {
	padding: 10px 0;
	background-color: #CCC49F;
	position: relative;
	clear: both;
}
/* ~~ 其他 float/clear 類別 ~~ */
.fltrt {  
	float: right;
	margin-left: 8px;
}
.fltlft { 
	float: left;
	margin-right: 8px;
}
.clearfloat { 
	clear:both;
	height:0;
	font-size: 1px;
	line-height: 0px;
}

/*HTML 5 支援 – 設定新的 HTML 5 標籤以顯示區塊並使瀏覽器能夠正確顯示標籤。 */
header, section, footer, aside, article, figure {
	display: block;
}
-->
</style>
<div id="header">    
<div id="menu">   
	<ul class="menu">        
        <li><a href="http://macrotech.herokuapp.com/"><span>首頁</span></a></li>
        <li><a href="content.php" target="_self"><span>重大歷程</span></a></li>
		<li><a href="content-is.php" target="_self"><span>資訊工程</span></a></li>
		<li><a href="content-green.php" target="_self"><span>綠能機電</span></a></li>
		<li><a href="content-eng.php" target="_self"><span>機電工程</span></a></li>
		<li><a href="content-sales.php" target="_self"><span>業務</span></a></li>
		<li><a href="http://www.104.com.tw/jobbank/custjob/index.php?r=cust&j=4070422648363f683c583a1d1d1d1d5f2443a363189j48&jobsource=checkc" target="_new"><span>人力資源</span></a></li>
		<li><a href="contact-us.php" target="_self"><span>連絡我們</span></a></li>
        <li><a href="#"><span>　　　　　</span></a></li>
        <li><a href="#"><span>　　　　　</span></a></li>    	
    </ul>
</div><br><br><br><br><br><br><br>
<div class="main" style="overflow: auto; border: solid 0px red; scrollbar-face-color: white; font-size: 12px; text-align: left; font-family: '新細明體';">
            
    <div id="MainContent_UpdatePanel1">
	
            <div id="MainContent_div_List">
                <fieldset>
                    <legend>                    <strong><img src="http://www.so-power.com/Img/point.gif" alt="" style="width: 17px; height: 15px" />業務部</strong>:<br>
                    <br>
                  </legend>
                    <table width="98%" border="1" align="center" cellpadding="1" cellspacing="1">
                      <tbody>
                        <tr>
                          <td valign="top" width="172"><p><span lang="EN-US"><u></u></span><strong><img src="http://www.so-power.com/Img/point.gif" alt="" style="width: 17px; height: 15px" /></strong>業務部</p></td>
                          <td valign="top" width="277"><p>營業能力服務項目 <span lang="EN-US"><u></u><u></u></span></p></td>
                          <td valign="top" width="162"><p><span lang="EN-US"><span style="text-align: center"> </span></span><span style="text-align: center">English Reference</span></p></td>
                        </tr>
                        <tr>
                          <td width="172" height="160" valign="top">
<img src="http://macrotech.herokuapp.com/00015.jpg" width="172" height="156" /></td>
                          <td valign="top" width="277"><p><img src="http://www.so-power.com/Img/point.gif" alt="" style="width: 17px; height: 15px" />國際貿易<span lang="EN-US">/</span>代購 <span lang="EN-US">&amp; </span>進出口服務<span lang="EN-US"> </span><span lang="EN-US"><u></u><u></u></span></p>
                            <p><img src="http://www.so-power.com/Img/point.gif" alt="" style="width: 17px; height: 15px" />各國商品代理銷售<span lang="EN-US"><u></u><u></u></span></p>
                            <p><img src="http://www.so-power.com/Img/point.gif" alt="" style="width: 17px; height: 15px" />企業團購<span lang="EN-US">: <a href="http://203.67.97.176/" target="_new"><span>旭購網</span></a><u></u><u></u></span><span lang="EN-US"><u></u><u></u></span></p>
                          <p><img src="http://www.so-power.com/Img/point.gif" alt="" style="width: 17px; height: 15px" />網路銷售<span lang="EN-US"><u></u><u></u></span></p></td>
                          <td valign="top" width="162"><p><span lang="EN-US">Int&rsquo;l    procurement and trading svc.<u></u><u></u></span></p>
                            <p><span lang="EN-US">Int&rsquo;l Rep. Sales<u></u><u></u></span></p>
                            <p><span lang="EN-US">Corporation EC svc <u></u><u></u></span></p>
                          <p><span lang="EN-US">Internet sale and marketing</span></p></td>
                        </tr>
                      </tbody>
                  </table>
                    <br>
                    <table width="98%" border="1" align="center" cellpadding="1" cellspacing="1">
                      <tbody>
                        <tr>
                          <td valign="top" width="172"><p><span lang="EN-US"><u></u></span><strong><img src="http://www.so-power.com/Img/point.gif" alt="" style="width: 17px; height: 15px" /></strong>資訊服務課</p></td>
                          <td valign="top" width="277"><p>營業能力服務項目 <span lang="EN-US"><u></u><u></u></span></p></td>
                          <td valign="top" width="162"><p><span lang="EN-US"><span style="text-align: center"> </span></span><span style="text-align: center">English Reference</span></p></td>
                        </tr>
                        <tr>
                          <td width="172" height="160" valign="top"><img src="http://macrotech.herokuapp.com/00016.jpg" alt="" width="172" height="156" /></td>
                          <td valign="top" width="277"><p><img src="http://www.so-power.com/Img/point.gif" alt="" style="width: 17px; height: 15px" />資訊網通商品代理經銷<span lang="EN-US"> </span><span lang="EN-US"><u></u><u></u></span></p>
                            <p><img src="http://www.so-power.com/Img/point.gif" alt="" style="width: 17px; height: 15px" /><span lang="EN-US"><u></u><u></u></span>企業資訊網路規劃建置</p>
                            <p><img src="http://www.so-power.com/Img/point.gif" alt="" style="width: 17px; height: 15px" />電腦、伺服器、 軟硬體、 週邊等銷售與技術服務</p>
                          <p>&nbsp;</p></td>
                          <td valign="top" width="162"><p><span lang="EN-US">PC</span><span lang="EN-US"> </span>、<span lang="EN-US">NB</span>、<span lang="EN-US">Server</span><span lang="EN-US"> </span><span lang="EN-US">sale and svc technical support</span></p>
                            <p><span lang="EN-US">Networking hardware and software support</span></p>
                          <p><span lang="EN-US">Troubleshooting and maintain svc. </span></p></td>
                        </tr>
                      </tbody>
                    </table>
                  <legend><br>
                  </legend>
                </fieldset><br>
<div class="footer">Copyright &copy; 1986 - 2012 旭宣系統科技(股)公司</div>
            </div>
        
</div>

        </div>
</div>
<div id="jquery"><a href="http://apycom.com/">.</a></div>
</body>
</html>